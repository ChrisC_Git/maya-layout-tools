# Maya Layout Tools

## About

Initially created as part of a training course for new Layout Artists using vanilla maya and no proprietary tools.

A set of very simple scripts to help a Layout Artist perform thier day to day task of Creating Cameras for Director Review and for the Animation Department.

## Scripts
1) Camera Rig Creator
2) Sequence to Shot Breakout for Animation (assumes you have used the Camera Rig Creator from this Repository)

## Installation & Usage