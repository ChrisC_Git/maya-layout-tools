##########################################################
# This is a Simple Script to Build a Quick Camera Rig
# for Maya 2022. Its bare bones with simple scripting to
# help beginners learn layout & scripting.
# ChrisC - 2022
# Working to Scale this camera might need to be 10x bigger
# please enter your scene scale below -> scene_scale
##########################################################

from maya import cmds

# WHAT SCALE DO YOU WORK TOO??
# Enter a value here, scale of 10 effectively makes Maya work in cm so a cube of 200 = 200cm = 2m
scene_scale = 10

# Create useful Functions
def UnlockAttribs(namespace, controller, *argv):
    '''
    here we are going to lock all attribs except the ones supplied to the function
    as *argv since most of the time we need them locked
    '''
    controls = ['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'visibility']
    
    # difference of the two lists, basically just the controls we want to lock
    new_list = []
    for control in controls:
        if control not in argv:
            new_list.append(control)
    print(new_list)

    # Lock All
    for control in new_list:
        cmds.setAttr( namespace+':' + controller + '.'+control, lock=True, keyable=False, channelBox=False ) 

def changeColour(namespace, controller, color = (1,1,1)):
    '''
    Here we setup the colour for a selected controller
    '''
    rgb = ("R","G","B")
    cmds.setAttr(namespace+':'+ controller + "Shape.overrideEnabled",1)
    cmds.setAttr(namespace+':'+ controller + "Shape.overrideRGBColors",1)
    for channel, color in zip(rgb, color):
        cmds.setAttr(namespace+':'+ controller + "Shape.overrideColor%s" %channel, color)

# START BUILDING RIG HERE

# Open a Dialog box to ask user for Camera Name
# https://help.autodesk.com/cloudhelp/2016/ENU/Maya-Tech-Docs/CommandsPython/promptDialog.html
result = cmds.promptDialog(
		title='Camera Name',
		message='Enter Name:',
		button=['OK', 'Cancel'],
		defaultButton='OK',
		cancelButton='Cancel',
		dismissString='Cancel')

# If the Ok button is pushed we make the Camera Rig
if result == 'OK':
    CameraName = cmds.promptDialog(query=True, text=True)

    # https://help.autodesk.com/cloudhelp/2016/ENU/Maya-Tech-Docs/CommandsPython/namespace.html
    # https://help.autodesk.com/cloudhelp/2016/ENU/Maya-Tech-Docs/CommandsPython/setAttr.html
    cmds.namespace( set=":" ) # makes sure we create the name space at root level
    cmds.namespace( add=CameraName )
    cmds.namespace( set=CameraName )
    cmds.camera( n='renderCamera' )

    # make controls
    cmds.circle(nr=(0,0,1), c=(0, 0, 0), r=1, n='roll_ctl')
    cmds.circle(nr=(1,0,0), c=(0, 0, 0), r=1.6, sw=180, n='tilt_ctl')
    cmds.circle(nr=(0,1,0), c=(0, 0, 0), r=1.8, n='pan_ctl')
    cmds.circle(nr=(1,0,0), c=(0, -1.2, 0), r=1, n='crane_ctl')
    cmds.circle(nr=(0,1,0), c=(0, 0, 0), r=2, n='dolly_track_ctl')
    cmds.circle(nr=(0,1,0), c=(0, 0, 0), r=2.2, sections=1, n='COG_ctl')
    cmds.group(n='local_ctl')
    cmds.group(n='camera')
    cmds.rename(CameraName+':renderCamera1', CameraName+':renderCamera')
    
    # Change shapes so they a bit clearer
    cmds.setAttr(CameraName+':crane_ctl.scaleZ', 0.5)
    cmds.makeIdentity(CameraName+ ':crane_ctl', apply=True )
    
    # build hierachy
    cmds.parent(CameraName+':renderCamera', CameraName+':roll_ctl')
    cmds.parent(CameraName+':roll_ctl', CameraName+':tilt_ctl')
    cmds.parent(CameraName+':tilt_ctl', CameraName+':pan_ctl')
    cmds.parent(CameraName+':pan_ctl', CameraName+':crane_ctl')
    cmds.parent(CameraName+':crane_ctl', CameraName+':dolly_track_ctl')
    cmds.parent(CameraName+':dolly_track_ctl', CameraName+':COG_ctl')
    cmds.namespace(set=":") # jump back to root when done

    # Apply users scene scale
    for scale_controls in ('scaleZ', 'scaleX', 'scaleY'):
        cmds.setAttr(CameraName+':local_ctl.'+ scale_controls, scene_scale)
    cmds.makeIdentity( CameraName+ ':local_ctl', apply=True ) # Freeze Transform before locking
    
    # Lock & Unlock the Attribs we need on each controller
    UnlockAttribs(CameraName, 'renderCamera') # not unlocking anything
    UnlockAttribs(CameraName, 'roll_ctl', 'rz')
    UnlockAttribs(CameraName, 'tilt_ctl', 'rx')
    UnlockAttribs(CameraName, 'crane_ctl', 'ty')
    UnlockAttribs(CameraName, 'pan_ctl', 'ry')
    UnlockAttribs(CameraName, 'dolly_track_ctl', 'tx', 'tz')
    UnlockAttribs(CameraName, 'COG_ctl', 'tx', 'ty', 'tz', 'rx', 'ry', 'rz')
    UnlockAttribs(CameraName, 'local_ctl', 'tx', 'ty', 'tz', 'rx', 'ry', 'rz')
    
    # Add colours to controllers
    changeColour(CameraName, 'roll_ctl', (0,0,1)) # blue
    changeColour(CameraName, 'tilt_ctl', (1,0,0)) # red
    changeColour(CameraName, 'pan_ctl', (0,1,0)) # green
    changeColour(CameraName, 'crane_ctl', (0,1,0)) # green
    changeColour(CameraName, 'dolly_track_ctl', (0,1,1)) # purple
    changeColour(CameraName, 'COG_ctl', (1,1,0)) # yellow
    
cmds.select(clear=True)