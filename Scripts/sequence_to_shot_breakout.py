##########################################################
# This is a Simple Script to Offset the Anim keys on the
# Camera Rig for Maya 2022. It moves the keys from the
# Camera Sequencer Location to the frame 1 for anim.
# Chris Cunnington - 2022
##########################################################

from maya import cmds

# Confirm shot breakdown
result = cmds.confirmDialog( title='Confirm', message='This is destructive! Have you saved your Scene!', button=['Yes','No'], defaultButton='Yes', cancelButton='No', dismissString='No' )

# If the Yes button is pushed
if result == 'Yes':

    # 1: Get a list of shots on timeline, and thier start frames on global timeline
    allObjects = cmds.ls(l=True)
    for obj in allObjects:
        # https://download.autodesk.com/us/maya/2011help/CommandsPython/shot.html
        if cmds.nodeType(obj) == 'shot':
            # get shot details
            shot_start = cmds.shot(obj, q=True, st=True)
            shot_cam = cmds.shot(obj, q=True, cc=True)
            
            # 2: move the keys for each corrosponding camera to frame 1, basically by the negative of thier start frame on the global timeline
            namespace = shot_cam.split(":")[0] #removes cam grp name_space so i can set the value
            cam_controls = cmds.listRelatives(namespace+':camera', allDescendents=True)
            for control in cam_controls:
                if 'Shape' not in control: #ignore Shape Nodes
                    # https://help.autodesk.com/cloudhelp/2022/ENU/Maya-Tech-Docs/CommandsPython/keyframe.html
                    cmds.keyframe(control, edit=True,relative=True,timeChange=-shot_start) #,time=(min_timeline,max_timeline)
                    
            # 3: delete shots from the sequencer
            cmds.delete(obj)